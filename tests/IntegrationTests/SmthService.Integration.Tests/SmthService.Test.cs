using Xunit;

namespace SmthService.Integration.Tests
{
    public class SmthServiceTest
    {
        [Fact]
        public void FooSuccess()
        {
            const int a = 2, b = 3, expected = 5;

            ISmthService smthService = new SmthService();

            Assert.Equal(expected, smthService.Foo(a, b));
        }
    }
}
