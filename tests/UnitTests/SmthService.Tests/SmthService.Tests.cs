using Moq;
using Xunit;

namespace SmthService.Tests
{
    public class SmthServiceTests
    {
        private readonly Mock<ISmthService> _mockSmthService;

        public SmthServiceTests()
        {
            _mockSmthService = new Mock<ISmthService>();
        }

        [Fact]
        public void FooSuccess()
        {
            _mockSmthService.Setup(service => service.Foo(It.IsAny<int>(), It.IsAny<int>())).Returns((int a, int b) => a + b);

            const int a = 2, b = 3, expected = 5;

            Assert.Equal(expected, _mockSmthService.Object.Foo(a, b));
        }
    }
}
